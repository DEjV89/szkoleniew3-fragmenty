package pl.androidwpraktyce.szkoleniew3_fragmenty;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by dkrezel on 2015-05-09.
 */
public class SzczegolyActivity extends ActionBarActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_szczegoly);

		// Odczytanie nazwy miasta z parametrów w Intent
		String mNazwaMiasta = getIntent().getStringExtra(SzczegolyFragment.MIASTO_KEY);

		SzczegolyFragment mSzczegolyFragment = new SzczegolyFragment();
		Bundle mBundle = new Bundle();
		mBundle.putString(SzczegolyFragment.MIASTO_KEY, mNazwaMiasta);
		mSzczegolyFragment.setArguments(mBundle);

		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_szczegoly, mSzczegolyFragment).commit();
	}

}
