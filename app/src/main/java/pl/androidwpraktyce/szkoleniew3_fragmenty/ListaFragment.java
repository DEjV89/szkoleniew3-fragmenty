package pl.androidwpraktyce.szkoleniew3_fragmenty;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.security.InvalidParameterException;

/**
 * Created by dkrezel on 2015-05-09.
 */
public class ListaFragment extends Fragment {

	private ListView mListView;
	private ListaParentInterface mParent;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// Sprawdzamy czy aktywnośc implementuje odpowiedni interfejs
		if (activity instanceof ListaParentInterface) {
			// Jeżeli tak ,to przypisujemy do zmiennej typu interface
			mParent = (ListaParentInterface) activity;
		} else {
			// W przeciwnym wypadku wyrzucamy wyjątek
			throw new InvalidParameterException("Aktywność nie implementuje odpowiedniego interfejsu");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// Zwalniamy zmieinna interfejsu
		mParent = null;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// Do zmiennej container (ViewGroup) nie można dodawać widoków
		// Container jest dla inflatera do wymiarowania

		// false - na końcu aby nowo utworzony widok nie został od razu umieszczony w container
		View mView = inflater.inflate(R.layout.fragment_lista, container, false);

		return mView;
	}

	// Wywołana po onCreateView
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		// Zmienna view to jest to samo co utworzyliśmy w metodzie onCreateView
		super.onViewCreated(view, savedInstanceState);	// Nie usuwać tej linikji !!!

		mListView = (ListView) view.findViewById(R.id.lista);
		ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getLista());
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				// adapterView - cała lista która obsługujemy
				// view - poszczególny element listy (obecnie kliknięty)
				// getAdapter() - pobranie aktualnego adaptera obslugującego listę
				//.getItem(position) - pobranie z adaptera elementu (obiektu) na pozycji ze zmiennej position
				String nazwaMiasta = (String) adapterView.getAdapter().getItem(position);

				mParent.openCityDetails(nazwaMiasta);
			}
		});
	}

	public String[] getLista() {
		return new String[] {"Warszawa", "Wrocław", "Poznań", "Katowice", "Gdańsk"};
	}

	public interface ListaParentInterface {
		public void openCityDetails(String mCity);
	}
}
