package pl.androidwpraktyce.szkoleniew3_fragmenty;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity implements ListaFragment.ListaParentInterface {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ListaFragment mListaFragment = new ListaFragment();

		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_lista, mListaFragment).commit();
		// Sprawdzenie czy istnieje FrameLayout do wyświatlenia szczegółó
		if (findViewById(R.id.fragment_szczegoly) != null) {
			SzczegolyFragment mSzczegolyFragment = new SzczegolyFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.fragment_szczegoly, mSzczegolyFragment).commit();
		}
	}

	@Override
	public void openCityDetails(String mCity) {
		// Otwarcie fragmentu szczegółów, powinniśmy przekazać parametr z nazwą miast
		SzczegolyFragment mSzczegolyFragment = new SzczegolyFragment();
		Bundle mArguments = new Bundle();
		mArguments.putString(SzczegolyFragment.MIASTO_KEY, mCity);
		mSzczegolyFragment.setArguments(mArguments);

		if (findViewById(R.id.fragment_szczegoly) != null) {
			// Jezeli tak to wyświetl w nim fragment szczegółów
			getSupportFragmentManager().beginTransaction().replace(R.id.fragment_szczegoly, mSzczegolyFragment).commit();
		} else {
			// Uruchomienie fragmentu w ramach tej samej Aktywności (wariant 1)
			// Jeżeli nie to nałóż na fragment listy fragment szczegółów
//			getSupportFragmentManager().beginTransaction()
//					.add(R.id.fragment_lista, mSzczegolyFragment, "Fragment szczegółów")
//					.addToBackStack(null)
//					.commit();

			// Uruchomienie nowej Aktywności z fragmentem (wariant 2 otwierania fragmentu)
			Intent mIntent = new Intent(this, SzczegolyActivity.class);
			mIntent.putExtra(SzczegolyFragment.MIASTO_KEY, mCity);
			startActivity(mIntent);

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
