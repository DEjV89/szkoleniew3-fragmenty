package pl.androidwpraktyce.szkoleniew3_fragmenty;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by dkrezel on 2015-05-09.
 */
public class SzczegolyFragment extends Fragment {

	private TextView mCityText;
	public static final String MIASTO_KEY = "miasto.key";

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View mView = inflater.inflate(R.layout.miasta_layout, container, false);

		return mView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mCityText = (TextView) view.findViewById(R.id.city_text);
		if (getArguments() != null) {
			mCityText.setText(getArguments().getString(MIASTO_KEY, "Brak przekazanego miasta"));
		}

	}
}
